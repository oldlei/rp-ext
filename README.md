<h3 align=”center”>This repo holds some redpill extensions, use at your own risk</h3>
<h3 align=”center”>Test the extensions and if they do not work as intended please open an issue</h3>
Extension Name: "aacraid" Description : "Adds  Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/aacraid/rpext-index.json">LINK</a><br>
Extension Name: "aic94xx" Description : "Adds Adaptec aic94xx SAS/SATA driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/aic94xx/rpext-index.json">LINK</a><br>
Extension Name: "alx" Description : "Adds Qualcomm Atheros(R) AR816x/AR817x PCI-E Ethernet Network Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/alx/rpext-index.json">LINK</a><br>
Extension Name: "arcmsr" Description : "Adds Areca ARC11xx/12xx/16xx/188x SAS/SATA RAID Controller Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/arcmsr/rpext-index.json">LINK</a><br>
Extension Name: "asix" Description : "Adds ASIX AX8817X based USB 2.0 Ethernet Devices Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/asix/rpext-index.json">LINK</a><br>
Extension Name: "atl1" Description : "Adds Atheros L1 Gigabit Ethernet Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/atl1/rpext-index.json">LINK</a><br>
Extension Name: "atl1c" Description : "Adds Qualcomm Atheros 100/1000M Ethernet Network Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/atl1c/rpext-index.json">LINK</a><br>
Extension Name: "atl1e" Description : "Adds Atheros 1000M Ethernet Network Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/atl1e/rpext-index.json">LINK</a><br>
Extension Name: "atl2" Description : "Adds Atheros Fast Ethernet Network Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/atl2/rpext-index.json">LINK</a><br>
Extension Name: "atlantic" Description : "Adds Marvell (Aquantia) Corporation(R) Network Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/atlantic/rpext-index.json">LINK</a><br>
Extension Name: "ax88179_178a" Description : "Adds ASIX AX88179/178A based USB 3.0/2.0 Gigabit Ethernet Devices Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/ax88179_178a/rpext-index.json">LINK</a><br>
Extension Name: "be2net" Description : "Adds Emulex OneConnect NIC Driver 10.6.0.3 Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/be2net/rpext-index.json">LINK</a><br>
Extension Name: "bna" Description : "Adds QLogic BR-series 10G PCIe Ethernet driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/bna/rpext-index.json">LINK</a><br>
Extension Name: "bnx2" Description : "Adds QLogic BCM5706/5708/5709/5716 Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/bnx2/rpext-index.json">LINK</a><br>
Extension Name: "bnx2x" Description : "Adds QLogic BCM57710/57711/57711E/57712/57712_MF/57800/57800_MF/57810/57810_MF/57840/57840_MF Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/bnx2x/rpext-index.json">LINK</a><br>
Extension Name: "cxgb" Description : "Adds Chelsio 10Gb Ethernet Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/cxgb/rpext-index.json">LINK</a><br>
Extension Name: "cxgb3" Description : "Adds Chelsio T3 Network Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/cxgb3/rpext-index.json">LINK</a><br>
Extension Name: "cxgb4" Description : "Adds Chelsio T4/T5/T6 Network Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/cxgb4/rpext-index.json">LINK</a><br>
Extension Name: "cxgb4vf" Description : "Adds Chelsio T4/T5/T6 Virtual Function (VF) Network Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/cxgb4vf/rpext-index.json">LINK</a><br>
Extension Name: "dm9601" Description : "Adds Davicom DM96xx USB 10/100 ethernet devices Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/dm9601/rpext-index.json">LINK</a><br>
Extension Name: "e1000" Description : "Adds Intel(R) PRO/1000 Network Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/e1000/rpext-index.json">LINK</a><br>
Extension Name: "e1000e" Description : "Adds Intel(R) PRO/1000 Network Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/e1000e/rpext-index.json">LINK</a><br>
Extension Name: "Early telnet start" Description : "Simple extension which starts telnet early at boot"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/early-telnet/rpext-index.json">LINK</a><br>
Extension Name: "forcedeth" Description : "Adds  Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/forcedeth/rpext-index.json">LINK</a><br>
Extension Name: "hpsa" Description : "Adds  Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/hpsa/rpext-index.json">LINK</a><br>
Extension Name: "hv_netvsc" Description : "Adds Microsoft Hyper-V network driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/hv_netvsc/rpext-index.json">LINK</a><br>
Extension Name: "i40e" Description : "Adds Intel(R) Ethernet Connection XL710 Network Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/i40e/rpext-index.json">LINK</a><br>
Extension Name:  Description : 
<a href=>LINK</a><br>
Extension Name: "iavf" Description : "Adds Intel(R) Ethernet Adaptive Virtual Function Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/iavf/rpext-index.json">LINK</a><br>
Extension Name: "igb" Description : "Adds Intel(R) Gigabit Ethernet Network Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/igb/rpext-index.json">LINK</a><br>
Extension Name: "igbvf" Description : "Adds Intel(R) Gigabit Virtual Function Network Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/igbvf/rpext-index.json">LINK</a><br>
Extension Name: "ixgbe" Description : "Adds Intel(R) 10GbE PCI Express Linux Network Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/ixgbe/rpext-index.json">LINK</a><br>
Extension Name: "ixgbe" Description : "Adds Intel(R) 10GbE PCI Express Linux Network Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/ixgbe.vanilla/rpext-index.json">LINK</a><br>
Extension Name: "ixgbevf" Description : "Adds Intel(R) 10 Gigabit Virtual Function Network Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/ixgbevf/rpext-index.json">LINK</a><br>
Extension Name: "megaraid_sas" Description : "Adds Avago MegaRAID SAS Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/megaraid_sas/rpext-index.json">LINK</a><br>
Extension Name: "mlx4_core" Description : "Adds  Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/mlx4_core/rpext-index.json">LINK</a><br>
Extension Name: "mlx5_core" Description : "Adds Mellanox Connect-IB, ConnectX-4 core driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/mlx5_core/rpext-index.json">LINK</a><br>
Extension Name: "mpt2sas" Description : "Adds  Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/mpt2sas/rpext-index.json">LINK</a><br>
Extension Name: "mpt3sas" Description : "Adds LSI MPT Fusion SAS 3.0 Device Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/mpt3sas/rpext-index.json">LINK</a><br>
Extension Name: "mptsas" Description : "Adds Fusion MPT SAS Host driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/mptsas/rpext-index.json">LINK</a><br>
Extension Name: "mptspi" Description : "Adds Fusion MPT SPI Host driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/mptspi/rpext-index.json">LINK</a><br>
Extension Name: "mvsas" Description : "Adds Marvell 88SE6440 SAS/SATA controller driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/mvsas/rpext-index.json">LINK</a><br>
Extension Name: "nct6775" Description : "Adds Driver for NCT6775F and compatible chips Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/nct6775/rpext-index.json">LINK</a><br>
Extension Name: "qla2xxx" Description : "Adds  Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/qla2xxx/rpext-index.json">LINK</a><br>
Extension Name: "qlcnic" Description : "Adds QLogic 1/10 GbE Converged/Intelligent Ethernet Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/qlcnic/rpext-index.json">LINK</a><br>
Extension Name: "r8101" Description : "Adds RealTek RTL-8101 Fast Ethernet driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/r8101/rpext-index.json">LINK</a><br>
Extension Name: "r8125" Description : "Adds Realtek RTL8125 2.5Gigabit Ethernet driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/r8125/rpext-index.json">LINK</a><br>
Extension Name: "r8152" Description : "Adds Realtek RTL8152/RTL8153 Based USB Ethernet Adapters Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/r8152/rpext-index.json">LINK</a><br>
Extension Name: "r8168" Description : "Adds RealTek RTL-8168 Gigabit Ethernet driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/r8168/rpext-index.json">LINK</a><br>
Extension Name: "r8169" Description : "Adds RealTek RTL-8169 Gigabit Ethernet driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/r8169/rpext-index.json">LINK</a><br>
Extension Name: "Early telnet start" Description : "Simple extension which starts telnet early at boot"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/raspbi-pill/rpext-index.json">LINK</a><br>
Extension Name: "redpill" Description : "Adds  Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/redpill/rpext-index.json">LINK</a><br>
Extension Name: "ACPI Daemon v2" Description : "ACPI Daemon v2 that handles power button events"
<a href="https://github.com/jumkey/redpill-load/raw/develop/redpill-acpid/rpext-index.json">LINK</a><br>
Extension Name: "RedPill Bootwait" Description : "Simple extension which stops the execution early waiting for the boot device to appear"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/redpill-boot-wait/rpext-index.json">LINK</a><br>
Extension Name: "rtl8150" Description : "Adds rtl8150 based usb-ethernet driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/rtl8150/rpext-index.json">LINK</a><br>
Extension Name: "sfc" Description : "Adds  Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/sfc/rpext-index.json">LINK</a><br>
Extension Name: "skge" Description : "Adds SysKonnect Gigabit Ethernet driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/skge/rpext-index.json">LINK</a><br>
Extension Name: "sky2" Description : "Adds Marvell Yukon 2 Gigabit Ethernet driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/sky2/rpext-index.json">LINK</a><br>
Extension Name: "Tinycore Diag" Description : "An extension that will assist in collecting DSM diagnostic information"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/tcrp-diag/rpext-index.json">LINK</a><br>
Extension Name: "tg3" Description : "Adds Broadcom Tigon3 ethernet driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/tg3/rpext-index.json">LINK</a><br>
Extension Name: "tn40xx" Description : "Adds  Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/tn40xx/rpext-index.json">LINK</a><br>
Extension Name: "v9fs" Description : "Adds  Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/v9fs/rpext-index.json">LINK</a><br>
Extension Name: "via-rhine" Description : "Adds VIA Rhine PCI Fast Ethernet driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/via-rhine/rpext-index.json">LINK</a><br>
Extension Name: "via-velocity" Description : "Adds VIA Networking Velocity Family Gigabit Ethernet Adapter Driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/via-velocity/rpext-index.json">LINK</a><br>
Extension Name: "vmw_pvscsi" Description : "Adds VMware PVSCSI driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/vmw_pvscsi/rpext-index.json">LINK</a><br>
Extension Name: "vmxnet3" Description : "Adds VMware vmxnet3 virtual NIC driver Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/vmxnet3/rpext-index.json">LINK</a><br>
Extension Name: "vxge" Description : "Adds Neterion's X3100 Series 10GbE PCIe I/OVirtualized Server Adapter Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/vxge/rpext-index.json">LINK</a><br>
Extension Name: "wch" Description : "Adds  Support"
<a href="https://gitea.com/oldlei/rp-ext/raw/main/wch/rpext-index.json">LINK</a><br>
<br><br><br>
<h3>Extensions will be updated upon request, please open an issue to include extensions</h3>
